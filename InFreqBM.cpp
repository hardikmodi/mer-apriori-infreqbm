/* 
 * File:   InFreqBM.cpp
 * Author: aeon
 * 
 * Created on 24 January, 2013, 8:08 PM
 */

#include "InFreqBM.h"
#include <set>
#include <math.h>
#include <iostream>
#include "subsetGen.h"

using namespace std;

InFreqBM::InFreqBM(int n) 
{ 
    countMaxInFreq=0;    
    bitMap=NULL;
    bitMap = new bool[(long int)pow(2,n)];
    for(int i=0;i<(long int)pow(2,n);i++)
    {
        bitMap[i]=false;
    }
    countItems=n;
}

long int InFreqBM::cardinality()
{
    return countMaxInFreq;
}


int InFreqBM::getIndex(set<int> & s)
{
    int num=0;
    set<int>::iterator itr;
    for (itr=s.begin();itr!=s.end();itr++)
    {
        if (((*itr)>=0)&&((*itr)<=countItems))
                num = num | ((unsigned int)1 << (*itr));
        else
            num=0;
    }    
    return num;
}

InFreqBM::~InFreqBM() 
{
    delete [] bitMap;       
    bitMap=NULL;
}

void InFreqBM::add(set<int>& s )
{            
    int indx=0;
    countMaxInFreq++;
    indx=getIndex(s);
    bitMap[indx]=true;    
    subsetGen ssg(s);    
    ssg.getNextSubset();    
    while(ssg.buf.size()>0)
    {                      
        indx=getIndex(ssg.buf);       
        bitMap[indx]=true;        
        ssg.getNextSubset();
    }   
}

bool  InFreqBM::subsetCheck(set<int>& s)
{
    int idx;
    idx=getIndex(s);
    if (bitMap[idx]==1)
        return true;
    else
        return false;
}
void InFreqBM::stub(set<int> &s)
{    
    cout<<"\n\nHASH MAP:\n";
    for (int i=0;i<pow(2,countItems);i++)
    {
        cout<<i<<"\t";
        if (bitMap[i]==true)
            cout<<"1"<<endl;
        else
            cout<<"0"<<endl;
    }    
}

